################################################################################
#
# picolinux
#
################################################################################

PICOLINUX_VERSION = master
PICOLINUX_SOURCE = picolinux-$(PICOLINUX_VERSION).tar.gz
PICOLINUX_SITE = https://gitlab.com/DrQuark/picolinux/-/archive/master

$(eval $(cmake-package))

