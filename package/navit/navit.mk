################################################################################
#
# NAVIT
#
################################################################################

NAVIT_VERSION = rpi
NAVIT_SITE = https://github.com/cedricp/navit/archive
NAVIT_SOURCE = $(NAVIT_VERSION).zip
NAVIT_LICENSE = LGPL-2.1+
NAVIT_LICENSE_FILES = COPYING
# NAVIT_INSTALL_STAGING = YES
NAVIT_CONF_OPTS = -DNO_GLES_X11=ON -DDISABLE_QT=ON -DBUILD_MAPTOOL=OFF

define NAVIT_EXTRACT_CMDS
	unzip $(NAVIT_DL_DIR)/$(NAVIT_SOURCE) -d $(@D)_
	mv $(@D)_/navit-$(NAVIT_VERSION)/* $(@D)
	rm -rf $(@D)_
endef

$(eval $(cmake-package))
