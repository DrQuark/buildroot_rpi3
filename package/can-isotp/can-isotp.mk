################################################################################
#
# apr
#
################################################################################

CAN_ISOTP_VERSION = master
CAN_ISOTP_SITE = $(call github,hartkopp,can-isotp,$(CAN_ISOTP_VERSION))
CAN_ISOTP_LICENSE = GPL-2
CAN_ISOTP_LICENSE_FILES = COPYING
CAN_ISOTP_MODULE_MAKE_OPTS = EXTRA_CFLAGS=-I$(CAN_ISOTP_DIR)/include/uapi

$(eval $(kernel-module))
$(eval $(generic-package))
