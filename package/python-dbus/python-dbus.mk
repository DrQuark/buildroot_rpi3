################################################################################
#
# python-dbus
#
################################################################################

PYTHON_DBUS_VERSION = 1.2.8
PYTHON_DBUS_SOURCE = dbus-python-$(PYTHON_DBUS_VERSION).tar.gz
PYTHON_DBUS_SITE = https://dbus.freedesktop.org/releases/dbus-python
PYTHON_DBUS_LICENSE = MIT
PYTHON_DBUS_LICENSE_FILES = LICENSE

ifeq ($(BR2_PACKAGE_PYTHON),y)
PYTHON_DBUS_CONF_OPTS = PYTHON_LIBS="-L$(STAGING_DIR)/usr/lib32 -lpython2.7" PYTHON_INCLUDES="-I$(STAGING_DIR)/usr/include/python2.7"
PYTHON_DBUS_DEPENDENCIES += python host-python
PYTHON=$(HOST_DIR)/bin/python2
endif

$(eval $(autotools-package))
